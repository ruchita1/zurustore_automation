package com.zuru.qa.testcase;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.zuru.qa.base.TestBaseClass;
import com.zuru.qa.pages.HomePage;
import com.zuru.qa.pages.LoginPage;
import com.zuru.qa.pages.ProjectDetailPage;
import com.zuru.qa.pages.UploadProjectPage;

public class HomePageTest extends TestBaseClass{
	LoginPage loginpage_obj;
	HomePage homepage_obj;
	UploadProjectPage uploadpage_obj;
	ProjectDetailPage projdetail_obj;
	public HomePageTest() throws IOException {
		super();
		//PageFactory.initElements(driver, this);
	}

	@BeforeMethod
	public void setUp() throws IOException, InterruptedException {
		initialization();
		loginpage_obj = new LoginPage();
		homepage_obj = new HomePage();
		uploadpage_obj = new UploadProjectPage();
		projdetail_obj = new ProjectDetailPage();   
		homepage_obj = loginpage_obj.login(prop.getProperty("username"), prop.getProperty("password"));
	}

	@Test
	public void PageTitleTest() throws InterruptedException {
		Thread.sleep(5000);
		String title = homepage_obj.validatePageTitle();
		assertEquals(title, "Store");
	}

	@Test
	public void BestProjectListTest() throws InterruptedException {
		Thread.sleep(3000);
		String best_label = homepage_obj.validateBestProjectList();
		assertEquals(best_label, "Best");
	}

	@Test
	public void ProjLikeTest() throws InterruptedException {
		assertTrue(homepage_obj.likeProject(),"Project doesn't like");
	}

	@Test
	public void ProjectSearchTest() {
		projdetail_obj = homepage_obj.SearchProj();
	}

	@Test
	public void UploadBtnTest() throws InterruptedException, IOException {
		uploadpage_obj = homepage_obj.upload();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
