package com.zuru.qa.testcase;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.zuru.qa.base.TestBaseClass;
import com.zuru.qa.pages.EditProjectDetail;
import com.zuru.qa.pages.HomePage;
import com.zuru.qa.pages.LoginPage;
import com.zuru.qa.pages.UploadProjectPage;
import com.zuru.qa.util.TestUtil;

public class EditProjectDetailTest extends TestBaseClass{

	LoginPage loginpage_obj;
	HomePage homepage_obj;
	UploadProjectPage uploadpage_obj;
	EditProjectDetail editprojdetail_obj;
	String Proj_Profile = "C:\\Users\\ZTI\\eclipse-workspace - Copy\\FreeZuruTest\\src\\main\\java\\com\\zuru\\qa\\testdata\\Proj_Profile.png";
	String Screenshot = "C:\\Users\\ZTI\\eclipse-workspace - Copy\\FreeZuruTest\\src\\main\\java\\com\\zuru\\qa\\testdata\\Screenshot.jpg";

	public EditProjectDetailTest() throws IOException {
		super();
	}

	@BeforeMethod
	public void setUp() throws IOException, InterruptedException {
		initialization();
		loginpage_obj = new LoginPage();
		homepage_obj = new HomePage();
		uploadpage_obj = new UploadProjectPage();
		editprojdetail_obj = new EditProjectDetail();   
		homepage_obj = loginpage_obj.login(prop.getProperty("username"), prop.getProperty("password"));
		uploadpage_obj = homepage_obj.upload();
		editprojdetail_obj = uploadpage_obj.ChooseProjectToUpload();
	}
	@DataProvider(name = "PublishProject")
	public Object[][] getTestData() throws EncryptedDocumentException, IOException {
		Object[][] data = TestUtil.getTestData();
		return data;
	}

	@Test(dataProvider = "PublishProject")
	public void EditProjectDetailPageTest(String Typology, String Project_Name, String HomeArea, String Bathrooms, String Rooms, String Floors,String Description) throws InterruptedException, IOException {
		editprojdetail_obj.EditProjectData(Typology, Project_Name, HomeArea, Bathrooms, Rooms, Floors, Description,Proj_Profile, Screenshot);
		editprojdetail_obj.publishProject();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
