package com.zuru.qa.testcase;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.zuru.qa.base.TestBaseClass;
import com.zuru.qa.pages.HomePage;
import com.zuru.qa.pages.LoginPage;

public class LoginPageTest extends TestBaseClass{
	LoginPage loginpage_obj;
	HomePage homepage_obj;
	public LoginPageTest() throws IOException {
		super();
	}

	@BeforeSuite
	public void SetUp() throws IOException {
		initialization();
		loginpage_obj = new LoginPage();
	}
	@Test
	public void LoginPageTitleTest() {
		String title = loginpage_obj.validateLoginPageTitle();
		assertEquals(title, "Auth");
	}
	@Test
	public void loginTest() throws InterruptedException, IOException {
		homepage_obj = loginpage_obj.login(prop.getProperty("username"), prop.getProperty("password"));
	}

	@AfterSuite
	public void tearDown() {
		driver.quit();
	}
}
