package com.zuru.qa.testcase;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.zuru.qa.base.TestBaseClass;
import com.zuru.qa.pages.EditProjectDetail;
import com.zuru.qa.pages.HomePage;
import com.zuru.qa.pages.LoginPage;
import com.zuru.qa.pages.UploadProjectPage;

public class ChooseProjectTest extends TestBaseClass{
	LoginPage loginpage_obj;
	HomePage homepage_obj;
	UploadProjectPage uploadpage_obj;
	EditProjectDetail editprojdetail_obj;

	public ChooseProjectTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod
	public void setUp() throws IOException, InterruptedException {
		initialization();
		loginpage_obj = new LoginPage();
		homepage_obj = new HomePage();
		uploadpage_obj = new UploadProjectPage();
		editprojdetail_obj = new EditProjectDetail();   
		homepage_obj = loginpage_obj.login(prop.getProperty("username"), prop.getProperty("password"));
		uploadpage_obj = homepage_obj.upload();
	}

	@Test
	public void ChooseProjectForUpload() throws InterruptedException, IOException {
		editprojdetail_obj = uploadpage_obj.ChooseProjectToUpload();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}


}
