package com.zuru.qa.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.zuru.qa.base.TestBaseClass;

public class TestUtil extends TestBaseClass{

	public TestUtil() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	public static long implicite_Wait_time = 10;
	public static long page_Load_time = 20;
	
	public static String s = "./src/main/java/com/zuru/qa/testdata/ProjectData.xlsx";
	static Workbook book;
	static org.apache.poi.ss.usermodel.Sheet sheet;
	
	public static Object[][] getTestData() throws EncryptedDocumentException, IOException{
		FileInputStream file = new FileInputStream(s);
		book  = WorkbookFactory.create(file);
		sheet = book.getSheet("publish");
		Object[][] data= new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for(int i =0; i<sheet.getLastRowNum();i++) {
			for(int j=0;j<sheet.getRow(0).getLastCellNum();j++) {
					data[i][j] = sheet.getRow(i+1).getCell(j).toString();
			}
		}
		return data;
	}
	public static void takeScreenshotAtEndOfTest() throws IOException {
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));
		
	}

}
