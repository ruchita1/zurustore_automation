package com.zuru.qa.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.zuru.qa.util.TestUtil;
import com.zuru.qa.util.WebEventListener;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBaseClass {
	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String currentDir = System.getProperty("user.dir");

	public TestBaseClass() throws IOException {
		prop = new Properties();
		FileInputStream ip = new FileInputStream("./src/main/java/com/zuru/qa/config/config.properties");
		prop.load(ip);
	}

	public static void initialization() throws IOException {
		String browser = prop.getProperty("browser");
		if(browser.equals("chrome")){
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		e_driver = new EventFiringWebDriver(driver);
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().pageLoadTimeout(TestUtil.page_Load_time, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.implicite_Wait_time,TimeUnit.SECONDS);
	}
}
