package com.zuru.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.zuru.qa.base.TestBaseClass;

public class ProjectsPage extends TestBaseClass{

	public ProjectsPage() throws IOException {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "react-autosuggest__input")
	WebElement SearchTextBox;

	@FindBy(id = "react-autowhatever-1")
	WebElement auto_sugestion;
	
	
	public ProjectDetailPage SearchProj(String proj_name) { 
		SearchTextBox.sendKeys(proj_name);
		auto_sugestion.click();
		return new ProjectDetailPage();
	}
	
	
	

	
}
