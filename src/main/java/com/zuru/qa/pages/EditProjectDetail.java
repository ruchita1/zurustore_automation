package com.zuru.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.zuru.qa.base.TestBaseClass;

public class EditProjectDetail extends TestBaseClass{

	public EditProjectDetail() throws IOException {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(name ="name")
	WebElement Project_name_element;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div/div[2]/div/div/div[1]/div[2]/div[1]/div[1]/div[1]/div/div[1]/div/div[2]/div/input")
	WebElement Project_image_element;
	
	@FindBy(id="upload-screenshot")
	WebElement Screenshot_element;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div/div[2]/div/div/div[1]/div[2]/div[1]/div[1]/div[1]/div/div[2]/div[1]/div/div/div/div/select/optgroup[1]/option[1]")
	WebElement Topology;

	
	@FindBy(name = "homeArea")
	WebElement homeArea_element;
	
	@FindBy(name = "bathrooms")
	WebElement bathrooms_element;
	
	@FindBy(name = "rooms")
	WebElement rooms_element;
	
	@FindBy(name = "floors")
	WebElement floors_element;
	
	@FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='Font'])[1]/following::div[30]")
	WebElement Desc_element;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div/div[2]/div/div/div[1]/div[2]/div[2]/button")
	WebElement publish_btn;
		
	public void EditProjectData(String typology, String Project_Name, String HomeArea, String Bathrooms,String Rooms, String  Floors, String Description, String Proj_Profile, String Screenshot) throws InterruptedException
	{
		Actions action = new Actions(driver);
		Project_image_element.sendKeys(Proj_Profile);
		Screenshot_element.sendKeys(Screenshot);
		Project_name_element.clear();
		Project_name_element.sendKeys(Project_Name);
		Topology.click();
		homeArea_element.sendKeys(HomeArea);
		bathrooms_element.sendKeys(Bathrooms);
		rooms_element.sendKeys(Rooms);
		floors_element.sendKeys(Floors);
		Desc_element.click();
        action.sendKeys(Description).perform();
	}
	
	public ProjectsPage publishProject() throws IOException {
		publish_btn.click();
		return new ProjectsPage();
	}
	
	
	

}
