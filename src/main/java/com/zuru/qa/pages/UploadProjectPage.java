package com.zuru.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.zuru.qa.base.TestBaseClass;

public class UploadProjectPage extends TestBaseClass{
	
	public UploadProjectPage() throws IOException {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[@id=\"wrapper\"]/div/div[2]/div/div[2]/div[3]/div[1]/div/div[1]/div/div[1]")
	WebElement project_id;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/button")
	WebElement Choose_button;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div/div[2]/div/div/div[1]/div[2]/div[2]/button")
	WebElement publish;
	
	public EditProjectDetail ChooseProjectToUpload() throws InterruptedException, IOException {
		Thread.sleep(1000);
		project_id.click();
		Choose_button.click();
		return new EditProjectDetail();
	}
}
