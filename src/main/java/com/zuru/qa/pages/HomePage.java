package com.zuru.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.zuru.qa.base.TestBaseClass;

public class HomePage extends TestBaseClass{
	//UploadProjectPage upload_obj;
	//ProjectDetailPage projdetail_obj;
	Boolean b;
	public HomePage() throws IOException {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/div/div[1]/div/div[1]/h2")
	WebElement Best;

	@FindBy(className = "react-autosuggest__input")
	WebElement SearchTextBox;

	@FindBy(id = "react-autowhatever-1")
	WebElement auto_sugestion;

	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div[1]/a/i")
	WebElement upload_btn;

	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div/div/div[2]/div/div/div/div[1]/div/div[1]/div/div/div/div[1]/div/i")
	WebElement like_btn;
	
	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div[2]/div/div[1]/ul/li[1]/a/i")
	WebElement home_btn;

	public String validatePageTitle() {
		return driver.getTitle();
	}

	public String validateBestProjectList() {
		return Best.getText();
	}

	public UploadProjectPage upload() throws InterruptedException, IOException {
		upload_btn.click();
		Thread.sleep(5000);
		return new UploadProjectPage();
	}

	public boolean likeProject() throws InterruptedException {
		String likeClass_original = like_btn.getAttribute("class");
		Thread.sleep(1000);
		like_btn.click();
		Thread.sleep(1000);
		String likeClass_afterclick = like_btn.getAttribute("class");
		if(likeClass_original.equals(likeClass_afterclick)) {
			b = false;
		}
		else if(likeClass_original!=likeClass_afterclick) {
			if(likeClass_afterclick.equals("like-button sprite sprite-icon-store-redHeartFull24")) {
				b = true;
			}
			else if (likeClass_afterclick.equals("like-button sprite sprite-icon-store-whiteHeartEmpty24")) {
				b = true; 
			}
			else {
				b = false;
			}
		}
		return b;
	}
	
	public ProjectDetailPage SearchProj() { 
		SearchTextBox.sendKeys("123dasd");
		auto_sugestion.click();
		return new ProjectDetailPage();
	}
}

