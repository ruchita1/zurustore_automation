package com.zuru.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.zuru.qa.base.TestBaseClass;

public class LoginPage extends TestBaseClass {
	//HomePage hm;
	public LoginPage() throws IOException {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div[2]/div[4]/a/div/button/div/div")
	WebElement login_btn;

	@FindBy(id = "login-input-field")
	WebElement uname;

	@FindBy(id = "passowrd-input-field")
	WebElement pwd;

	@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/div[2]/div[2]/div[3]/div")
	WebElement submit_btn;

	public String validateLoginPageTitle() {
		return driver.getTitle();
	}

	public HomePage login(String username, String password) throws InterruptedException, IOException {
		login_btn.click();
		uname.sendKeys(username);
		pwd.sendKeys(password);
		submit_btn.click();
		//Thread.sleep(7000);
		return new HomePage();
	}
}
